#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <cstring>
#include <signal.h>

#include "global.h"
#include "client.h"

const std::string procfile(std::string("/proc/") + std::string(PROCFILE));
const std::string pidfile("/tmp/kqueueclient");

void help()
{
 std::cout << "client [help | push_back message | pop_front | start_daemon | stop_daemon]" << std::endl;
}

int push_back(const std::string & msg)
{
 std::ofstream out;

 out.open(procfile.c_str());
 if (!out.fail())
 {
  out << msg;
  out.close();
  return(1);
 }
 else
 {
  return (-1);
 }
}

int pop_front(std::ostream &out)
{
 int ret(0);
 std::ifstream in;
 char buf[MAXMESSAGESIZE];

 in.open(procfile.c_str());
 if (in.fail())
 {
  std::cout << "Error: file " << procfile << " is not open !" << std::endl;
  return (-1);
 }

 in.read(buf, MAXMESSAGESIZE);
 if (in.gcount() > 0)
 {
  out << buf;
  ret = 1;
 }

 in.close();
 return (ret);
}

void start_daemon()
{
 if (!check_pid_file())
 {
  std::cout << "Error: Daemon are already started" << std::endl;
  return;
 }

 int pid = fork();
 if (pid == -1)
 {
  std::cout << "Error: Start Daemon failed" << std::endl << std::strerror(errno) << std::endl;
  return;
 }
 else if (!pid) 
 {
  write_pid_file();
  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  {
   struct sigaction sa;
   memset (&sa, 0, sizeof(sa));
   sa.sa_handler = signal_handler;
   sigaction(SIGUSR1, &sa, NULL);
   sigaction(SIGTERM, &sa, NULL);
   sigaction(SIGHUP,  &sa, NULL);
  }

  daemon();
 }
}

void daemon()
{
 int num(0);
 while(true)
 {
  std::stringstream fname, msg;
  std::ofstream out;

  sleep(5);

  pop_front(msg);
  if (!msg.str().empty())
  {
   fname << "/tmp/client" << std::setw(4) << std::setfill('0') << std::dec << num;
   out.open(fname.str().c_str());
   if (!out.fail())
   {
    out << msg.str();
    out.close();
   }
   if (num++ > 1024) {num = 0;}
  }
 }
}

void stop_daemon()
{
 int pid = read_pid_file();
 if (pid <= 0 || check_pid_file())
 {
  std::cout << "Error: Daemon are not running" << std::endl;
  return;
 }
 kill(pid, SIGUSR1);
}

int main(int argc, char **argv)
{
 if (argc <= 1)
 {
  help();
  return (0);
 }

 std::string param(argv[1]);

 if (param == std::string("help"))
 {
  help();
  return(0);
 }

 if (param == std::string("pop_front"))
 {
  int r = pop_front();
  if (r < 0)
  {
   std::cout << "Error: file " << procfile << " is not open !" << std::endl;
  }
  if (r == 0)
  {
   std::cout << "Queue is empty." << std::endl;
  }
  if (r > 0)
  {
   std::cout << std::endl;
  }
  return(0);
 }

 if (param == std::string("start_daemon"))
 {
  start_daemon();
  return(0);
 }

 if (param == std::string("stop_daemon"))
 {
  stop_daemon();
  return(0);
 }

 if (argc >= 3)
 {
  std::string msg(argv[2]);

  if (param == std::string("push_back"))
  {
   int r = push_back(msg);
   if (r > 0)
   {
    std::cout << "push back " << msg << std::endl;
   }
   if (r < 0)
   {
    std::cout << "Error: file " << procfile << " is not open !" << std::endl;
   }
   return(0);
  }
 }

 std::cout << "Error: Unknown command." << std::endl << "use \"client help\" for help." << std::endl;
 return(0);
}

const int read_pid_file()
{
 int ret(-1);
 std::ifstream fin;
 fin.open(pidfile);
 if (!fin.fail())
 {
  std::string s;
  fin >> s;
  if (s.length() > 0)
  {
   ret = std::atoi(s.c_str());
  }
 }
 fin.close();
 return(ret);
}

const bool check_pid_file()
{
 std::ifstream fin; std::stringstream stm;
 bool ret(true);
 int pid;

 pid = read_pid_file();

 if (pid > 0)
 {
  stm.setf(std::ios::dec);
  stm << "/proc/" << pid << "/stat";
  fin.open(stm.str());

  if (!fin.fail())
  {
   std::string str;
   bool r(false);
   while(!(fin.rdstate() & std::ios::eofbit) && !r)
   {
    fin >> str;
    r = str.find("client") != std::string::npos;
    if (!r) ret = false;
   }
   fin.close();
  }
 }

 return(ret);
}

const bool write_pid_file()
{
 bool ret(true);
 std::ofstream fout;

 fout.open(pidfile, std::ios::binary | std::ios::trunc);

 if (fout.fail())
 { ret = false; }
 else
 {
  fout.setf(std::ios::dec);
  fout << getpid();
 }

 fout.close();
 return (ret);
}

void signal_handler(int signum)
{
 (void) signum;
 std::remove(pidfile.c_str());
 exit(0);
}
