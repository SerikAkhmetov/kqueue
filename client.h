#ifndef KCLIENTH
#define KCLIENTH

#include <iostream>
#include <string>

void help();
int push_back(const std::string & msg);
int pop_front(std::ostream &out = std::cout);
void start_daemon();
void daemon();
void stop_daemon();

int main(int argc, char **argv);

const int read_pid_file();
const bool check_pid_file();
const bool write_pid_file();

void signal_handler(int signum);

#endif
