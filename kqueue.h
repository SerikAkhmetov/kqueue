
#ifndef KQUEUEH
#define KQUEUEH

struct q_item
{
 struct q_item * next;
 struct q_item * prev;
 char * message;
 size_t length;
 char   file_name[9];
};

void pop_front(void);
void push_back(struct q_item *);
const size_t q_size(void);
void q_clear_(void);

struct q_item * create_item(void);
void delete_item(struct q_item *);

void save(struct q_item *);
ssize_t restore(struct q_item *);

static ssize_t proc_read(struct file *, char __user *, size_t, loff_t *);
static ssize_t proc_write(struct file *, const char __user *, size_t, loff_t *);

int q_start_thread(void);
int q_thread(void *);

struct file *file_open(const char *path, int flags, int rights);

#endif
