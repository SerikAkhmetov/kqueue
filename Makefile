CURRENT = $(shell uname -r)
KDIR = /lib/modules/$(CURRENT)/build
PWD = $(shell pwd)

obj-m := kqueue.o

default:
	$(MAKE) -C $(KDIR) M=$(PWD) modules

clean: 
	@rm -f *.o .*.cmd .*.flags *.mod.c *.order 
	@rm -f .*.*.cmd *~ *.*~ TODO.* 
	@rm -fR .tmp*
	@rm -rf .tmp_versions
	@rm -rf /tmp/client*
disclean: clean
	@rm *.ko *.symvers client

client: client.o
	g++ -W -o client client.o
client.o: client.cpp client.h global.h
	g++ client.cpp -c -o client.o
