#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/proc_fs.h>
#include <linux/string.h>
#include <linux/mutex.h>
#include <linux/sched.h>
#include <linux/kthread.h>
#include <linux/delay.h>
#include <linux/semaphore.h>
#include <linux/fs.h>

#include "global.h"
#include "kqueue.h"

MODULE_LICENSE("GPL" );
MODULE_AUTHOR("Serik Akhmetov serik@rambler.ru" );

static struct q_item * first, * last;
static struct proc_dir_entry * procfile;
static int eof;
static int item_count;
struct task_struct * thread;
static DEFINE_MUTEX(q_lock);

static ssize_t proc_read(struct file * file, char __user * data, size_t length, loff_t * off)
{
 ssize_t ret = 0;

 printk("+ proc_read: size=%d", length);

 if (mutex_lock_interruptible(&q_lock)) return -ERESTARTSYS;

 if (eof == 1)
 {
  eof = 0;
  mutex_unlock(&q_lock);
  return(0);
 }

 if (first != NULL)
 {
  ssize_t count = first->length > length ? length : first->length;

  if (first->file_name[0] != 0)
  {
   restore(first);
  }

  ret = count - copy_to_user(data, first->message, count);

  printk("+ proc_read: data=%s\n", first->message);

  file->f_pos += ret;

  pop_front(); 
 }

 printk("+ proc_read: ret=%d\n", ret);

 eof = ret > 0 ? 1 : 0; 

 mutex_unlock(&q_lock);
 return(ret);
}

static ssize_t proc_write(struct file * file, const char __user * data, size_t length, loff_t * off)
{
 struct q_item * item;

 printk("+ proc_write: size=%d data=%s\n", length, data);

 item = create_item();
 if (item == NULL)
 {
  printk("+ error: push_back kmalloc\n");
  return -ERESTARTSYS;
 }

 item->length = length < MAXMESSAGESIZE ? length : MAXMESSAGESIZE;
 item->message = (char *)vmalloc(item->length);
 if (item->message == NULL)
 {
  printk("+ error: push_back not memory for message!\n");
  delete_item(item);
  return -ERESTARTSYS;
 }
 if (copy_from_user(item->message, data, item->length) != 0)
 {
  delete_item(item);
  printk("+ error: push_back copy_from_user!\n");
  return -ERESTARTSYS;
 }

 push_back(item);

 return(item->length);
}

static const struct file_operations proc_file_fops = {
 .owner = THIS_MODULE,
 .read  = proc_read,
 .write = proc_write,
};

static int __init q_init( void )
{
 printk("+ module kqueue start!\n" );
 first = NULL;
 last = NULL;
 eof = 0;
 item_count = 0;
 procfile = (struct proc_dir_entry *)proc_create(PROCFILE, S_IRUGO|S_IWUSR, NULL, &proc_file_fops);
 if (procfile == NULL)
 {
  printk("+ proc_create error\n");
  return (-1);
 }

 q_start_thread();
 //thread = NULL;

 return 0;
}

static void __exit q_exit(void)
{
 printk("+ module kqueue unloaded!\n");

 if (thread != NULL)
 {
  printk("+ queue task stopped!\n");
  kthread_stop(thread);
 }

 remove_proc_entry(PROCFILE, NULL);

 q_clear_();
}

module_init(q_init);
module_exit(q_exit);

void pop_front(void)
{
 struct q_item * item;
 if (first != NULL)
 {
  item = first;
  first = first->next;
  delete_item(item);
 }
 if (first == NULL)
 {
  last = NULL;
 }
}

void push_back(struct q_item * item)
{
 while(q_size() >= MAXQUEUELENGTH) { pop_front(); }

 mutex_lock(&q_lock);
 if (last == NULL)
 {
  item->prev = NULL;
  item->next = NULL;
  last = item;
  first = item;
 }
 else
 {
  item->prev = last;
  item->next = NULL;
  last->next = item;
  last = item;
 }
 mutex_unlock(&q_lock);
}

const size_t q_size(void)
{
 size_t s = 0;
 struct q_item * item;
 mutex_lock(&q_lock); 
 for(item = first; item != NULL; item = item->next) { s++; }
 mutex_unlock(&q_lock);
 return(s);
}

void q_clear_(void)
{
 struct q_item * item;

 mutex_lock(&q_lock);

 for(item = first; item != NULL;)
 {
  struct q_item * prev;
  
  prev = item;
  item = item->next;

  vfree(prev->message);
  kfree(prev);
 }

 first = NULL;
 last = NULL;

 mutex_unlock(&q_lock);
}

int q_start_thread(void)
{
 thread = kthread_run(q_thread, NULL, "queue_task");
 if (IS_ERR(thread))
 {
   printk("+ Creating an queue thread failed.\n");
   return PTR_ERR(thread);
 }

 return (0);
}

int q_thread(void * data)
{
 printk("+ Queue thread started.\n");

 while(!kthread_should_stop())
 {
  msleep(100);
  if (!mutex_lock_interruptible(&q_lock))
  {
   struct q_item * item;
   for(item = first; item != NULL; item = item->next)
   {
    if (item->file_name[0] == 0)
    {
     save(item);
     break;
    }
   }
   mutex_unlock(&q_lock);
  }
 }

 return (0);
}

void save(struct q_item * item)
{
 struct file * out;
 char full_name[32];
 loff_t offset = 0;

 if (item->file_name[0] != 0) return;
 sprintf(item->file_name, "item%04d", item_count);

 if (item_count++ > 1024) item_count = 0; 

 sprintf(full_name, "/tmp/%s", item->file_name);

 out = file_open(full_name, O_WRONLY|O_CREAT, 0644);

 if (out == NULL)
 {
  item->file_name[0] = 0;
  printk("+ Error write file full_name\n");
  return;
 }

 if (kernel_write(out, item->message, item->length, &offset) != item->length)
 {
  printk("+ Error write file full_name length failed\n");
 }
 else
 {
  vfs_fsync(out, 0);
  vfree(item->message);
  item->message = NULL;
 }
 filp_close(out, NULL);
}

ssize_t restore(struct q_item * item)
{
 struct file * in;
 char full_name[32];
 loff_t offset = 0;
 ssize_t ret = 0;

 if (item->file_name[0] == 0) return (ret);

 sprintf(full_name, "/tmp/%s", item->file_name);

 in = file_open(full_name, O_RDONLY, 0);

 if (in == NULL)
 {
  printk("+ Error read file %s\n", full_name);
  return(-1);
 }

 item->message = (char *)vmalloc(item->length);
 if (item->message == NULL)
 {
  printk("+ error: restore not memory for message!\n");
  return(-2);
 }

 ret = kernel_read(in, item->message, item->length, &offset);

 filp_close(in, NULL);

 return(ret);
}

struct q_item * create_item(void)
{
 struct q_item * item;

 item = (struct q_item *)kmalloc(sizeof(struct q_item), GFP_KERNEL);
 if (item == NULL)
 {
  printk("+ error: create_item kmalloc\n");
  return(NULL);
 }

 item->next = NULL;
 item->prev = NULL;
 item->message = NULL;
 item->length = 0;
 item->file_name[0] = 0;

 return(item);
}

void delete_item(struct q_item * item)
{
 if (item == NULL) return;
 if (item->message != NULL) vfree(item->message);
 kfree(item);
}

struct file *file_open(const char *path, int flags, int rights) 
{
    struct file *filp = NULL;
    mm_segment_t oldfs;
    int err = 0;

    oldfs = get_fs();
    set_fs(get_ds());
    filp = filp_open(path, flags, rights);
    set_fs(oldfs);
    if (IS_ERR(filp))
    {
        err = PTR_ERR(filp);
        return (NULL);
    }
    return (filp);
}

